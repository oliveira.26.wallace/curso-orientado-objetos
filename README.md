# Curso de CSharp completo

## Sumario
- ### [Descrição](#descrição)
- ### [Conteúdo do Curso](#conteúdo-do-curso-1)

# Descrição

<p>Esse repositório é para postar os códigos referente ao curso de <a href= "https://www.udemy.com/course/programacao-orientada-a-objetos-csharp/">C# COMPLETO 2020 Programação Orientada a Objetos + Projetos - Udemy</a>. Cada módulo do curso realizado será postado em uma branch diferente</p>

# Conteúdo do Curso
## Sumario

- ### [Lógica de Programação usando C#](#lógica-de-programação-usando-c-2)
    - #### [Descrição](#descrição-3)
    - #### [Tipos básicos de dados em C#](#descrição-2)
        - #### [Tipos valor](#tipos-valor-1)
        - #### [Tipos Refência](#tipos-refência-1)

## Lógica de Programação usando C#

### Descrição
<p> Server para contruir programas básicos, aplicando o básico de lógica de programação, na linguagem C#, nesse módulo foi estudado:</p>

- Tipos de dados básicos em C#
- Estrutura sequencial (entrada, processamento, saída)
- Operadores (aritméticos, comparativos, lógicos)
- Estruturas de controle (if-else, while, for) 

### Tipos básicos de dados em C#

#### Tipos valor

Tipo C# | Tipo Framework .Net   | Sinal     | Bytes     | Valores Possiveis
------- | -------------------   | -----     | -----     | -----------------
sbyte   | System.Sbyte          | Sim       | 1         | -128 até 127
short   | System.Int16          | Sim       | 2         | -32768 até 32767 
int     | System.Int32          | Sim       | 4         | -2<sup>31</sup> até 2<sup>31</sup> -1
long    | System.Int64          | Sim       | 8         | -2<sup>63</sup> até 2<sup>63</sup> -1
byte    | System.Byte           | Não       | 1         | 0 até 255
ushort  | System.Uint16         | Não       | 2         | 0 até 65535
uint    | System.Uint32         | Não       | 4         | 0 até 2<sup>32</sup>-1
ulong   | System.Uint64         | Não       | 8         | 0 até 2<sup>64</sup>-1
float   | System.Single         | Sim       | 4         | Aproximadamente 1.5 * 10<sup>-45</sup> até 3.4 * 10<sup>38</sup> com uma precisão de 7 dígitos
double  | System.Double         | Sim       | 8         | Aproximadamente 5.0 * 10<sup>-324</sup> até 1.7 × 10<sup>308</sup> com uma precisão de 15-16 dígitos 
decimal | System.Decimal        | Sim       | 12        | Aproximadamente 1.0 * 10<sup>-28</sup> até 7.9 * <sup>28</sup> com 28-29 dígitos significativos
char    | System.Char           | N/A       | 2         | Qualquer caracter do Unicode
bool    | System.Boolean        | N/A       | 1/2       | true ou false

#### Tipos Refência

Tipo C# | Tipo .NET     | Descrição
------- | ---------     | ----------
string  | System.String | Uma cadeia de caracteres Unicode IMUTÁVEL (segurança, simplicidade, thread safe)
object  | System.Object | Um objeto genérico (toda classe em C# é subclasse de object): GetType, Equals, GetHashCode, ToString
